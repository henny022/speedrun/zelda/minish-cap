# Kinstones
## kinds of Kinstones
- GP
- GE (G[)
- GC (G<)
- ...

## Routes
### any%
- 2 GP, 1 GE, 1 GC

## Manips
### Castor Wilds Single GP
- GP
- [Video](https://youtu.be/yG17JQgzAQE)
- Reset
- 3 dust
- charge sword
- spin 3 right bushes when fully charged

### Castor Wilds Double Manip
- GE, GP
- [Video](https://youtu.be/oMMRjX4Er0w)
- Reset
- Hold up-left
- Slash twice as soon as possible
- Slash the two bottom right bushes and hold your sword out (GE)
- Break the middle right bush
- Wait until the green clone bar is near the edge of the rocks
- Release your Spin Attack (GP)

### Castor Wilds Quad Manip
- GE, GP, GC, GP
- [Video](https://youtu.be/Y2y0luvvpH0)
- [Video with notes](https://youtu.be/Kfbib_kJ-HI)

### Lon Lon Ranch Manip
- GC, GC, GP
- [Video](https://youtu.be/Jy5NplofqDw)
- Enter LLR from Lake Hylia
- Equip the Mole Mitts and save and quit.
- Mash the Mole Mitts as soon as you gain control of Link (GC)
