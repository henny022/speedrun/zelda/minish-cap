# Any%
## rupee route
- 20 bottle
- 40 grip ring
- 60 waking mushroom

## Prep
- create new file
- set text to fast (first option to the right)
- soft reset

## run
- blupee manip
- leave and go through the festival

## Earth Element
- go to the forest
- go to the temple to trigger ezlo cutscene
- get ezlo
- enter minish village
- go to temple
- go to barrel
- go to elder
- enter temple
- do temple
  - turn barrel in reverse
- pickup red rupee before the boss if needed (< 25)
- kill boss
- pickup Element

## Fire Element
- talk to elder
- get bombs
  - pull bomb on bridge for timing
- get windcrest at home
- go to hyrule
- kinstone intro
  - Y/N on second dialog
  - save and reset
- get spinattack
- buy bottle (20)
- go to mountain
  - pick up water on the way
- bombable wall with 15 rupees and fairies at the beginning
- get green water
- get red overworkd rupee on the way
- bombable wall cave
- green seed
- go right
- mushroom layer thing
- go left, up, right
- buy grip ring (40)
  - refill on bombs here if needed
- go left
  - cave with fairies and heart piece for refill at climb wall
- go up the wall and over the gap
- do rock puzzle
- go through caves
  - 20 rupees after switch
- go to smith
- enter dungeon

### Cavern
- go through the first floor
- go to minecart
- go left
  - use gust jar with bomb enemy to blow up the wall
- go minish route
- press switch
- get 50 rupee chest
- get key
- angle retention OOB
- silver chu fight
- cane of pacci
- left, up
- minish route, minecart, switch minecart
- angle stuff
  - put one enemy into the wall next to the door
  - put one into the far left hole
  - pull lever with down-left angle 6 times
  - push it up to the wall
  - pull it down-left into the wall once (use sword spin to get to it without loosing angle)
  - pull it up-left to teleport OOB 
- go up, left, up to the boss
- grab fairy
- fight gleerock

## Ocarina
- go to hyrule castle
- activate windcrest
- go down left stairs, up
- fuse Element
- leave castle
- vaati miniboss
- go to bottle cave
- go towards castor wilds
- read sign
  - enter and leave castor wilds if sign breaks
- go to shoe shop
- talk to a minish on the desk
- go to lon lon ranch
- get key from inside the house
- go to syrup
- buy shroom (60)
- go to shoe shop
- get pegasus boots
- back to castor wilds
- bow
- kinstone manips
- peahat clip
- run down
- run right then up

## Fortress of Winds
- get right side key
- go left keydoor
- grab bombs and heart after darknut
- get mole mitts
- soft reset
- go right side
- pickup red kinstone from the chest
- go to minish portal
- do portal items clip
- kill mazaal
- get Ocarina

# Flippers... what Flippers?
- fly to Tingle
- fuse with Tingle
- talk to Tingle
- fuse with wind normad
- fuse with blue Tingle
- fuse with David Jr.
- manip GC at lon lon if you don't have it
- fuse with purple Tingle
- pick up magic boomerang
- do Lakehouse OG
  - bombs and boots
  - run down while putting down a bomb
  - after bonking equip bow, hold up
  - shoot an arrow the roll
  - do OG off of the open door
- run to temple
- play Ocarina
- enter temple

## Temple of Droplets
- go left
- activate switch downstairs
- go up
- OG
- get boss key
- save and reset
- get small key
- OG
- do boomerang bossdoor skip
  - get the line with the two dots from the cane
  - otherwise go down a bit and roll back up
  - go right into the screen transition
  - hold up and mash start
  - go left, up, right to get the white screen transition
  - without the white screentransiton you need to play the ocarina once again
- to the scissor beetle fight
- push the first lever
- go to temple right side
- go through the ice path
- down through the key door
  - there is a backup key up
- blue chu fight
- lantern
- S+Q
- blue portal
- go downstairs
- OG
- down, through the dark room, left
- left under the gate wall thing
- left, up, right
- play ocarina and jump down
- go up and to the second level
- push the level
- fight octorock
  - phase one: three rocks
  - phase two: suck, rock, suck, two rocks, repeat

# into the sky
- fly to hyrule town
- get the threesword
- fly to south hyrule field
- go to the portal to the sky
- OG through the people
  - eu version needs to talk to the elder to move the second person, otherwise you are stuck after the dungeon

## Palace of Winds
- get the cape
- go up the clouds
- shortcut to the moving platform
- go left and up the clouds
- go with the moving platform, no clones needed
- stairclip
- get key
- wirbelhop
- go up the clouds
- kill ball and chain guys
- get boss key
- glitch boss door with lamp
  - lamp on b, press a and b
- screentransiton right, left, up
- go up till outside
- go right to the boss
- kill gyorg

# The Beginning of the End
- fly to hyrule city
- get four sword
- cutscene walking

## DHC
- left, up, up
- OG
- go back to the start and right
- go up, use cape to get out of the wall
- go down
- up the stairs
- exit and reenter through front door
- up, left
- follow the path to the key
- S+Q
- OG at left door
- go up the center
- go left to get on layer 2
- go down
- go left when there appears a room at the right
- jump down the door
- go up the stairs
- get key
- S+Q
- repeat to layer 2
- go down again
- go to that room on the right
- at the pattern on the wall, boot right and hold down
- jump down the doorframe
- go up the stairs
- get key
- go down once
- OG
- go in the door frame to the left
- jump over the door
  - jump, hold down-left
  - after the screen transition hold left
  - if you screentransiton accidentaly without jumping, go back and roll once to reenable jumping
- go up
- do the same door jump to the left
- kill ball and chain guys
- left, up
- bomb the blocks
- go up the grate
- activate switches
- get down again
  - jump, then roll into the wall on top of the moving platform
- right, up
- darknut fight
- down, right
- kill darknuts
- go up
- kill the real ghost
- get the key
- S+Q
- blue portal
- get boss key
- portal back
- red portal
- go through the bossdoor

## The End of the End
- jump over the ball and chain guy
- open the door with 4 clones
- kill darknuts
- go up
- vaati 1
- vaati 2
  - cycle 1 eyes are on the bottom
  - eyes are never in the same position twice
  - skip cycle 4 with spinning backwards then slash
- get zelda
- go to the minish gate
- vaati 3
